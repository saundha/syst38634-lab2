package time;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * 
 * @author Harpreet Saund
 *
 */
public class TimeTest {

	@Test
	public void getTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time does not match the expected result.", totalSeconds == 3661);
	}

	@Test(expected = NumberFormatException.class)
	public void getTotalSecondsException() {
		@SuppressWarnings("unused")
		int totalSeconds = Time.getTotalSeconds("01:01:0A");
		fail("Invalid time.");
	}

	@Test
	public void getTotalSecondsBoundryIn() {
		int totalSeconds = Time.getTotalSeconds("00:00:59");
		assertTrue("The time does not match the expected result", totalSeconds == 59);
	}

	@Test(expected = NumberFormatException.class)
	public void getTotalSecondsBoundryOut() {
		@SuppressWarnings("unused")
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		fail("Invalid time.");
	}
	
	// Get milliseconds tests.
	
	@Test
	public void getMillisecondsTestRegular() {
		assertTrue("Invalid time.", Time.getMilliseconds("12:05:05:05") == 5);
	}
	
	@Test(expected = NullPointerException.class)
	public void getMillisecondsTestException() {
		@SuppressWarnings("unused")
		int totalMilliseconds = Time.getMilliseconds(null);
		fail("Invalid time.");
	}
	
	@Test
	public void getMillisecondsTestBoundryIn() {
		assertTrue("Invalid time.", Time.getMilliseconds("12:05:05:1000") == 1000);
	}
	
	@Test(expected = NumberFormatException.class)
	public void getMillisecondsTestBoundryOut() {
		@SuppressWarnings("unused")
		int totalMilliseconds = Time.getMilliseconds("12:05:05:-1");
		fail("Invalid time.");
	}

}
